Demonstration script for Debian bug #786609 http://bugs.debian.org/786609

The script sets up three directories:

 - aptroot: a root directory structure with /etc/apt, /var/lib/dpkg and
   /var/cache/apt to try out apt behaviour independent of the host system
 - exprepo: a repository with codename experimental and pin priority 500
 - sidrepo: a repository with codename unstable and pin priority 900

Then three calls to apt-get are done:

 - installing pkg-a normally
 - installing it with -t experimental
 - installing it with aspcud as resolver

See the Debian bug for more details.
