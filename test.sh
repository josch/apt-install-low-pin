#!/bin/sh

set -x

PWD="`pwd`"
APTROOT="$PWD/aptroot"
EXPREPO="$PWD/exprepo"
SIDREPO="$PWD/sidrepo"
ARCH="amd64"

rm -rf "$EXPREPO"
rm -rf "$SIDREPO"
rm -rf "$APTROOT"

# populate experimental repository

mkdir -p "$EXPREPO"

cat << END > "$EXPREPO/Packages"
Package: pkg-a
Version: 1.0
Architecture: amd64
Depends: pkg-b (= 2.0), pkg-c, pkg-d (= 1.0)

Package: pkg-b
Version: 2.0
Architecture: amd64

Package: pkg-c
Version: 2.0
Architecture: amd64

Package: pkg-d
Version: 2.0
Architecture: amd64
END

md5=`md5sum "$EXPREPO/Packages" | awk '{print $1}'`
size=`stat -c %s "$EXPREPO/Packages"`

cat << END > "$EXPREPO/Release"
Codename: experimental
MD5Sum:
 $md5 $size Packages
END

# populate unstable repository

mkdir -p "$SIDREPO"

cat << END > "$SIDREPO/Packages"
Package: pkg-b
Version: 1.0
Architecture: amd64

Package: pkg-c
Version: 1.0
Architecture: amd64

Package: pkg-d
Version: 1.0
Architecture: amd64
END

md5=`md5sum "$SIDREPO/Packages" | awk '{print $1}'`
size=`stat -c %s "$SIDREPO/Packages"`

cat << END > "$SIDREPO/Release"
Codename: unstable
MD5Sum:
 $md5 $size Packages
END

# setup apt

mkdir -p $APTROOT
mkdir -p $APTROOT/etc/apt/
mkdir -p $APTROOT/etc/apt/apt.conf.d/
mkdir -p $APTROOT/etc/apt/preferences.d/
mkdir -p $APTROOT/var/lib/dpkg/
mkdir -p $APTROOT/var/cache/apt/

touch $APTROOT/var/lib/dpkg/status

cat << END > "$APTROOT/etc/apt/apt.conf"
Apt::Architecture "$ARCH";
Apt::Architectures "$ARCH";
Dir "$APTROOT/";
Dir::State::Status "$APTROOT/var/lib/dpkg/status";
Debug::pkgProblemResolver true;
Debug::pkgDepCache::Marker 1;
Debug::pkgDepCache::AutoInstall 1;
END

cat << END > "$APTROOT/etc/apt/sources.list"
deb file:$EXPREPO ./
deb file:$SIDREPO ./
END

cat << END > "$APTROOT/etc/apt/preferences.d/sid.pref"
Package: *
Pin: release n=unstable
Pin-Priority: 900
END

cat << END > "$APTROOT/etc/apt/preferences.d/exp.pref"
Package: *
Pin: release n=experimental
Pin-Priority: 500
END

APT_CONFIG="$APTROOT/etc/apt/apt.conf"
export APT_CONFIG

# run test

apt-get update

apt-cache policy

apt-get install --simulate pkg-a

apt-get install --simulate pkg-a -t experimental

apt-get install --simulate --solver aspcud \
	-o APT::Solver::Strict-Pinning=false \
	-o APT::Solver::aspcud::Preferences="-new,-removed,-changed,+sum(solution,apt-pin)" \
	pkg-a
